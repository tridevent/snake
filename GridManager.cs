﻿using UnityEngine;
using System.Collections;


public class GridManager : MonoBehaviour {
    public GameObject image;
    public GameObject[,] grid;
    public GameObject[,] newGrid; //objetos que tienes la info de cada celda
    //.....variables dynamics....
    public int width;
    public int height;
    public int scaleX;
    public int scaleY;
    public int offsetX;
    public int offsetY;

    public static GridManager instance;

    private GridManager()
    {
        instance = this;
    }

    void Start()
    {
        grid = new GameObject[width, height];
        newGrid = new GameObject[width, height];
        Vector2 myPosition;
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                myPosition = new Vector2((x *scaleX)+offsetX, (y * scaleY)+offsetY);
                grid [x, y] = image ;
                newGrid[x,y]=(GameObject) Instantiate(grid[x, y], new Vector3(myPosition.x, 0, myPosition.y), Quaternion.identity);
                newGrid[x,y].transform.parent = gameObject.transform;
                newGrid[x,y].transform.localPosition = new Vector3(myPosition.x,myPosition.y,0);
            }
        }
        GameManager.instance.CreateSnake();
    }
}
