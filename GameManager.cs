﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {
    public Vector3 inicialPosition;
    public Image cabeza;
    public Image cuerpo;
    public Image slot;
    public GameObject uiParent;
    public int sizeSnake1=2;
    public int sizeSnake2=2;
    public bool turn=false;// false player 1 , true player 2
    public List<Vector2> snakePosition1 = new List<Vector2>();
    public List<Vector2> snakePosition2 = new List<Vector2>();
    public int actionSnake=0;
    public GameObject[] Buttons;
    public int directionSnake1=0; // 0 left , 1 right , 2 up , 3 down
    public int directionSnake2=0;
    public int vertexSnake1 = 99;
    public int vertexSnake2 = 99;

    public static GameManager instance;

    private GameManager()
    {
        instance = this;
    }
    // Use this for initialization

    void Start () {
        //InvokeRepeating("Run", 1, 2);
    }
	
	// Update is called once per frame
	void Update () {
	if(Input.GetMouseButtonUp(0))
        {
            AddBodySnake(2);
        }
    if(Input.GetMouseButtonUp(1))
        {
            Run();
        }
	}
    public void CreateSnake()
    {
        GridManager.instance.newGrid[5, 5].GetComponent<Image>().sprite = cabeza.sprite;
        snakePosition1.Add( new Vector2(5, 5));
        GridManager.instance.newGrid[6, 5].GetComponent<Image>().sprite = cuerpo.sprite;
        snakePosition1.Add(new Vector2(6, 5));

        GridManager.instance.newGrid[6, 2].GetComponent<Image>().sprite = cabeza.sprite;
        snakePosition2.Add(new Vector2(6, 2));
        GridManager.instance.newGrid[7, 2].GetComponent<Image>().sprite = cuerpo.sprite;
        snakePosition2.Add(new Vector2(7, 2));
    }
    public void AddBodySnake(int player)
    {
        //.....Agrega un cuerpo al snake1
        if (player == 1)
        {
            snakePosition1.Add(new Vector2(snakePosition1[sizeSnake1-1].x + 1,snakePosition1[sizeSnake1-1].y));
            if (snakePosition1[sizeSnake1-1].x < GridManager.instance.width - 1)
            {
                sizeSnake1++;
                GridManager.instance.newGrid[(int)snakePosition1[sizeSnake1-1].x, (int)snakePosition1[sizeSnake1-1].y].GetComponent<Image>().sprite = cuerpo.sprite;
                Debug.Log("Agregado");
            }
            else
            {
                GameOver(1);
            }
            
        }
        else //.....Agrega un cuerpo al snake2
        {
        snakePosition2.Add(new Vector2(snakePosition2[sizeSnake2-1].x + 1,snakePosition2[sizeSnake2-1].y));
            if (snakePosition2[sizeSnake2-1].x < GridManager.instance.width)
            {
                sizeSnake2++;
                GridManager.instance.newGrid[(int)snakePosition2[sizeSnake2-1].x, (int)snakePosition2[sizeSnake2-1].y].GetComponent<Image>().sprite = cuerpo.sprite;
                Debug.Log("Agregado");
            }
            else
            {
                GameOver(2);
            }
            
        }
    }

    public void GameOver(int player)
    {
        //Debug.LogError(" el jugador " + player + " perdio");
        Application.LoadLevel("Lose");
    }
    public void Run()
    {
        turn = !turn;
        if (turn)
        {
            Buttons[0].SetActive(true);
            Buttons[1].SetActive(true);
            Buttons[2].SetActive(false);
            Buttons[3].SetActive(false);

            if (actionSnake == 0)
            {
                for (int d = 0; d < sizeSnake2; d++)
                {
                    #region directions
                    if (directionSnake2 == 0)
                    {
                        snakePosition2[d] = new Vector2(snakePosition2[d].x - 1, snakePosition2[d].y);
                    }
                    if(directionSnake2==1)
                    {
                        snakePosition2[d] = new Vector2(snakePosition2[d].x + 1, snakePosition2[d].y);
                    }
                    if (directionSnake2==2)
                    {
                    if(snakePosition2[d].y < GridManager.instance.height-1)//up limit
                        {
                            Debug.Log("d:" + d + "v:" + vertexSnake2);
                            /*if (d == vertexSnake2 && d < sizeSnake2 - 1)
                            {
                                snakePosition2[d + 1] = new Vector2(snakePosition2[d].x, snakePosition2[d].y);
                                Debug.Log("Esquina");
                            }
                            if(d<vertexSnake2)
                            {
                                snakePosition2[d] = new Vector2(snakePosition2[d].x, snakePosition2[d].y + 1);
                                Debug.Log("Up");
                            }
                            else
                            {
                                snakePosition2[d] = new Vector2(snakePosition2[d].x+1, snakePosition2[d].y);
                                Debug.Log("left");
                            }*/
                            if (vertexSnake2 < sizeSnake2 )
                            {
                                if (d >= vertexSnake2)
                                {
                                    snakePosition2[d] = new Vector2(snakePosition2[d].x + 1, snakePosition2[d].y);
                                    Debug.Log("block left");
                                }
                                if (d < vertexSnake2)
                                {
                                    snakePosition2[d] = new Vector2(snakePosition2[d].x, snakePosition2[d].y + 1);
                                    Debug.Log("block up");
                                }
                            }
                            else
                            {
                                snakePosition2[d] = new Vector2(snakePosition2[d].x, snakePosition2[d].y + 1);
                                Debug.Log("hacia arriba");
                            }

                        }
                    else
                        {
                            GameOver(2);
                        }
                    }
                    if (directionSnake2 == 3)
                    {
                        if (d == vertexSnake2)
                        {
                            snakePosition2[d] = new Vector2(snakePosition2[d].x + 1, snakePosition2[d].y -1);
                        }
                    }
                    #endregion
                    #region DrawSnake
                    //Debug.Log("pos " + snakePosition2[d]);
                    if (d == 0)
                    {
                        if (snakePosition2[0].x < 0)
                        {
                            GameOver(2);
                        }
                        else
                        {
                            GridManager.instance.newGrid[(int)snakePosition2[d].x, (int)snakePosition2[d].y].GetComponent<Image>().sprite = cabeza.sprite;
                            //Debug.Log("ini " + snakePosition2[d]);
                        }
                    }
                    if (d > 0 && d < sizeSnake2)
                    {
                        GridManager.instance.newGrid[(int)snakePosition2[d].x, (int)snakePosition2[d].y].GetComponent<Image>().sprite = cuerpo.sprite;
                        //Debug.Log("corp " + snakePosition2[d]);
                    }
                    #endregion
                    #region DeleteDraw
                    //.........................left
                    if (directionSnake2 == 0)
                    {
                        if (d == sizeSnake2 - 1)
                        {
                            GridManager.instance.newGrid[(int)snakePosition2[d].x + 1, (int)snakePosition2[d].y].GetComponent<Image>().sprite = slot.sprite;
                        }
                    }
                    //.........................right
                    if (directionSnake2 == 1)
                    {
                        if (d == sizeSnake2 - 1)
                        {
                            GridManager.instance.newGrid[(int)snakePosition2[d].x + 1, (int)snakePosition2[d].y].GetComponent<Image>().sprite = slot.sprite;
                        }
                    }
                    //.........................up
                    if (directionSnake2 == 2)
                    {
                        if (d == sizeSnake2 - 1)
                        {
                            if (d < sizeSnake2 - 1)
                            {
                                GridManager.instance.newGrid[(int)snakePosition2[d].x + 1, (int)snakePosition2[d].y].GetComponent<Image>().sprite = slot.sprite;
                            }
                            else
                            {
                                GridManager.instance.newGrid[(int)snakePosition2[d].x, (int)snakePosition2[d].y - 1].GetComponent<Image>().sprite = slot.sprite;
                            }

                        }
                    }
                    //.........................down
                    if (directionSnake2 == 3)
                    {
                        if (d == vertexSnake2 && d < sizeSnake2 - 1)
                        {
                            GridManager.instance.newGrid[(int)snakePosition2[d].x + 1, (int)snakePosition2[d].y].GetComponent<Image>().sprite = slot.sprite;
                        }
                    }
                    #endregion

                    Debug.Log("<b>SnakePosition2: "+snakePosition2[d]+"</b>");
                }
                Debug.Log("<b>.....</b>");
                if (vertexSnake2<sizeSnake2)
                {
                    vertexSnake2++;
                }
            }
            #region arriba2
            if (actionSnake==1)
            {
                actionSnake = 0;
                /*GridManager.instance.newGrid[(int)snakePosition2[0].x, (int)snakePosition2[0].y].GetComponent<Image>().sprite = slot.sprite;
                snakePosition2[0] = new Vector2(snakePosition2[0].x + 1, snakePosition2[0].y + 1);
                if (snakePosition2[0].y > GridManager.instance.height - 1)
                {
                    GameOver(2);
                }
                else
                {
                    GridManager.instance.newGrid[(int)snakePosition2[0].x, (int)snakePosition2[0].y].GetComponent<Image>().sprite = cabeza.sprite;
                    directionSnake2 = 2;
                    vertexSnake2 = -1;
                }*/
                directionSnake2 = 2;
                vertexSnake2 = 0;
            }
            #endregion
            #region Abajo2
            if (actionSnake ==2)
            {
                actionSnake = 0;
                GridManager.instance.newGrid[(int)snakePosition2[0].x, (int)snakePosition2[0].y].GetComponent<Image>().sprite = slot.sprite;
                snakePosition2[0] = new Vector2(snakePosition2[0].x + 1, snakePosition2[0].y - 1);
                if (snakePosition2[0].y <= 0)
                {
                    GameOver(2);
                }
                else
                {
                    GridManager.instance.newGrid[(int)snakePosition2[0].x, (int)snakePosition2[0].y].GetComponent<Image>().sprite = cabeza.sprite;
                    directionSnake2 = 3;
                }
                vertexSnake2 = 1;
            }
            #endregion 
        }
        else
        {
            Buttons[0].SetActive(false);
            Buttons[1].SetActive(false);
            Buttons[2].SetActive(true);
            Buttons[3].SetActive(true);

            if (actionSnake == 0)
            {
                for (int d = 0; d < sizeSnake1; d++)
                {
                    #region directions
                    if (directionSnake1 == 0)
                    {
                        snakePosition1[d] = new Vector2(snakePosition1[d].x - 1, snakePosition1[d].y);
                    }
                    if (directionSnake1 == 1)
                    {
                        snakePosition1[d] = new Vector2(snakePosition1[d].x + 1, snakePosition1[d].y);
                    }
                    if (directionSnake1 == 2)
                    {
                        if (d == vertexSnake1)
                        {
                            snakePosition1[d] = new Vector2(snakePosition1[d].x + 1, snakePosition1[d].y + 1);
                        }
                    }
                    if (directionSnake1 == 3)
                    {
                        if (d == vertexSnake1)
                        {
                            snakePosition1[d] = new Vector2(snakePosition1[d].x + 1, snakePosition1[d].y - 1);
                        }
                    }
                    #endregion
                    #region DrawSnake
                    //Debug.Log("pos " + snakePosition2[d]);
                    if (d == 0)
                    {
                        if (snakePosition1[0].x < 0)
                        {
                            GameOver(1);
                        }
                        else
                        {
                            GridManager.instance.newGrid[(int)snakePosition1[d].x, (int)snakePosition1[d].y].GetComponent<Image>().sprite = cabeza.sprite;
                            //Debug.Log("ini " + snakePosition2[d]);
                        }
                    }
                    if (d > 0 && d < sizeSnake1)
                    {
                        GridManager.instance.newGrid[(int)snakePosition1[d].x, (int)snakePosition1[d].y].GetComponent<Image>().sprite = cuerpo.sprite;
                        //Debug.Log("corp " + snakePosition2[d]);
                    }
                    if (d == sizeSnake1 - 1)
                    {
                        GridManager.instance.newGrid[(int)snakePosition1[d].x + 1, (int)snakePosition1[d].y].GetComponent<Image>().sprite = slot.sprite;
                        //Debug.Log("fin " + snakePosition2[d]);
                    }
                    #endregion

                }
                if (vertexSnake1 < sizeSnake1)
                {
                    vertexSnake1++;
                }
            }
            #region arriba2
            if (actionSnake == 1)
            {
                actionSnake = 0;
                GridManager.instance.newGrid[(int)snakePosition1[0].x, (int)snakePosition1[0].y].GetComponent<Image>().sprite = slot.sprite;
                snakePosition1[0] = new Vector2(snakePosition1[0].x + 1, snakePosition1[0].y + 1);
                if (snakePosition1[0].y > GridManager.instance.height - 1)
                {
                    GameOver(1);
                }
                else
                {
                    GridManager.instance.newGrid[(int)snakePosition1[0].x, (int)snakePosition1[0].y].GetComponent<Image>().sprite = cabeza.sprite;
                    directionSnake1 = 2;
                    vertexSnake1 = 1;
                }
            }
            #endregion
            #region Abajo2
            if (actionSnake == 2)
            {
                actionSnake = 0;
                GridManager.instance.newGrid[(int)snakePosition1[0].x, (int)snakePosition1[0].y].GetComponent<Image>().sprite = slot.sprite;
                snakePosition1[0] = new Vector2(snakePosition1[0].x + 1, snakePosition1[0].y - 1);
                if (snakePosition1[0].y <= 0)
                {
                    GameOver(1);
                }
                else
                {
                    GridManager.instance.newGrid[(int)snakePosition1[0].x, (int)snakePosition1[0].y].GetComponent<Image>().sprite = cabeza.sprite;
                    directionSnake1 = 3;
                }
                vertexSnake1 = 1;
            }
            #endregion 
        }
    }
    public void UpAction()
    {
        actionSnake = 1;
    }
    public void DownAction()
    {
        actionSnake = 2;
    } 
}
